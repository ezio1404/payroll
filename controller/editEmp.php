<?php
require '../model/empModel.php';
$emp=new Employee();

if(isset($_POST['editEmp'])){
$id=filter($_POST['emp_id']);
$fname=filter($_POST['emp_fname']);
$lname=filter($_POST['emp_lname']);
$bday=filter($_POST['emp_bdate']);
$hw=filter($_POST['emp_hw']);
$rate=filter($_POST['emp_rate']);
$ot=filter($_POST['emp_ot']);
$flag=true;
$empArray=array($id,$fname,$lname,$bday,$hw,$rate,$ot);

for($i=0;$i<count($empArray);$i++){
    if($empArray[$i] == ""){
        $flag = false;
        break;
    }
}

if($flag){
    $emp->updateEmployee($empArray,$id);
    header('location:../index.php?Succes_updating');
}
else{
    $message = "Invalid Credentials";
}

}

function filter($data){
    return htmlentities(trim($data));
}