<?php
require '../model/empModel.php';
$emp=new Employee();

if(isset($_POST['addEmp'])){
$id=filter($_POST['id']);
$fname=filter($_POST['fname']);
$lname=filter($_POST['lname']);
$bday=filter($_POST['bday']);
$hw=filter($_POST['hw']);
$rate=filter($_POST['rate']);
$ot=filter($_POST['ot']);
$flag=true;
$empArray=array($id,$fname,$lname,$bday,$hw,$rate,$ot);

for($i=0;$i<count($empArray);$i++){
    if($empArray[$i] == ""){
        $flag = false;
        break;
    }
}

if($flag){
    $emp->addEmployee($empArray);
    header('location:../index.php?Succes_adding');
}
else{
    $message = "Invalid Credentials";
}

}

function filter($data){
    return htmlentities(trim($data));
}