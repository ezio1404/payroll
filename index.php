<?php
require_once 'model/empModel.php';
$emp=new Employee();
$list=$emp->getAllEmployee();
if(isset($_GET['sortfname'])){
    $list=$emp->getAllEmployeeByFullname();
}
if(isset($_GET['sortage'])){
    $list=$emp->getAllEmployeeByBday();
}
if(isset($_GET['sortop'])){
    $list=$emp->getAllEmployeeByOP();
}
if(isset($_GET['search'])){
    $list=$emp->getAllEmployeeBySearch($_GET['search']);
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Payroll</title>
    <link href="view/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="view/assets/fonts/css/font-awesome.min.css" rel="stylesheet">
</head>

<body >

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="view/emp.php">Employee</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="container">
        <form method="get">
            <!-- <input class="form-control col-md-5" type="search" name="search" id="search" placeholder="Search"> -->
            <input ng-model="search" type="text" class="form-control col-md-5" placeholder="Search...">
        </form>
        <form  method="get">
            <input type="submit" value="Fullname" name="sortfname">
            <input type="submit" value="Age" name="sortage">
            <input type="submit" value="Overall Pay" name="sortop">      
        </form>
    <table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Full Name</th>
      <th scope="col">Age</th>
      <th scope="col">Reg Pay</th>
      <th scope="col">Hours Worked</th>
      <th scope="col">Over Time</th>
      <th scope="col">Total Pay</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
<?php
$today=date("d",strtotime(date('Y-m-d')));


foreach($list as $e){
?>
    <tr>
      <th scope="row"><?php echo $e['emp_id']?></th>
      <td><?php echo $e['emp_lname'].','.$e['emp_fname']?></td>
      <td><?php echo $today-date("d",strtotime($e['emp_bdate']));?></td>
      <td>PHP <?php echo $e['emp_rate']?></td>
      <td><?php echo $e['emp_hw']?></td>
      <td><?php echo $e['emp_ot']?></td>
      <td>PHP <?php echo number_format((($e['emp_rate']*$e['emp_hw'])+($e['emp_rate']*$e['emp_ot'])),2,'.','');
       ?></td>
      <td>
      <button type="button" id="edit" class="btn  btn-outline-primary" onclick="editModal('<?php echo $e['emp_id']; ?>');"><em>Edit</em></button>
      <button type="button" id="delete" class="btn  btn-outline-danger" onclick="deleteModal('<?php echo $e['emp_id']; ?>');"><em>Delete</em></button>
      </td>
    </tr>
    <?php
    }
    ?>
  </tbody>
</table>
    </div>


      <div class="modal fade bd-example-modal-lg" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Edit</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form method="post" action="controller/editEmp.php">
            <div class="form-group">
              <div class="form-row">
                  <label for="emp_id">Employee ID</label>
                  <input class="form-control" readonly name="emp_id" id="emp_id" type="number" >
                  <label for="emp_fname">Firstname</label>
                  <input class="form-control"  name="emp_fname" id="emp_fname" type="text" >
                  <label for="emp_lname">lastname</label>
                  <input class="form-control"  name="emp_lname" id="emp_lname" type="text" >
                  <label for="emp_bdate">Birthday</label>
                  <input class="form-control"  name="emp_bdate" id="emp_bdate" type="date" >
                  <label for="emp_hw">Hours Worked</label>
                  <input class="form-control"  name="emp_hw" id="emp_hw" type="number" >
                  <label for="emp_rate">Rate</label>
                  <input class="form-control"  name="emp_rate" id="emp_rate" type="number"  step=".01">
                  <label for="emp_ot">Over Time</label>
                  <input class="form-control"  name="emp_ot" id="emp_ot" type="number" > 
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <input type="submit" class="btn btn-outline-primary" name="editEmp" value="Save">
          <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
        </div>
        </form>
      </div>
    </div>
  </div>

<!-- -->
<div class="modal fade bd-example-modal-lg" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Delete</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form method="post" action="controller/deleteEmp.php">
            <div class="form-group">
              <div class="form-row">
   
        
                  <label for="emp_id1">Do you want to delete? </label>
                  <input class="form-control" readonly name="emp_id" id="emp_id1" type="text" >      

              </div>
            </div>
        </div>
        <div class="modal-footer">
          <input type="submit" class="btn btn-outline-warning" name="deleteEmp" value="Delete">
          <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
        </div>
        </form>
      </div>
    </div>
  </div>
</body>
<script src="view/assets/js/angular.min.js"></script>
<script src="view/assets/js/jquery.min.js"></script>
<script src="view/assets/js/bootstrap.min.js"></script>

  <script>
    function editModal(id) {
        $.ajax({
        url: 'controller/getEmp.php',
        type: 'get',
        data: {id: id},
        dataType: 'json',
        cache: false,
        success:function(res){
            console.log(res);
            $('#emp_id').val(res.emp_id);
            $('#emp_fname').val(res.emp_fname);
            $('#emp_lname').val(res.emp_lname);
            $('#emp_bdate').val(res.emp_bdate);
            $('#emp_hw').val(res.emp_hw);
            $('#emp_rate').val(res.emp_rate);
            $('#emp_ot').val(res.emp_ot);
             $('#editModal').modal('show');
        }
    });
    }
    function deleteModal(id1){
        $('#emp_id1').val(id1);
      console.log(id1)
      $('#deleteModal').modal();
    }

  </script>
</body>

</html>