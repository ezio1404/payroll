<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Payroll</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/fonts/css/font-awesome.min.css" rel="stylesheet">
</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="../index.php">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="emp.php">Employee</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="container">
        <form class="needs-validation" novalidate method="POST" action="../controller/addEmp.php">

            <div class="form-row">
                <div class="col-md-12 mb-3 ">
                    <label for="validationEmpId">Employee ID</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend"></span>
                        </div>
                        <input type="number" class="form-control" id="validationEmpId" placeholder="Employee ID"
                            aria-describedby="inputGroupPrepend" name="id" required>
                        <div class="invalid-feedback">
                            Please choose a username.
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mb-3 ">
                    <label for="validationCustomUsername">Firstname</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend"></span>
                        </div>
                        <input type="text" class="form-control" id="validationCustomUsername" placeholder="Firstname"
                            aria-describedby="inputGroupPrepend" name="fname" required>
                        <div class="invalid-feedback">
                            Please choose a Firstname.
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <label for="validationCustomPassword">Lastname</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend"></span>
                        </div>
                        <input type="text" class="form-control" id="validationCustomPassword" placeholder="Lastname"
                            aria-describedby="inputGroupPrepend" name="lname" required>
                        <div class="invalid-feedback">
                            Please input a Lastname.
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <label for="validationBday">Birthdate</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend"></span>
                        </div>
                        <input type="date" class="form-control" id="validationBday" placeholder="Birthdate"
                            aria-describedby="inputGroupPrepend" name="bday" required>
                        <div class="invalid-feedback">
                            Please input a Birthdate.
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <label for="validationHW">Hours Worked</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend"></span>
                        </div>
                        <input type="number" class="form-control" id="validationHW" placeholder="Hours Worked"
                            aria-describedby="inputGroupPrepend" name="hw" required>
                        <div class="invalid-feedback">
                            Please input a Hours Worked.
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <label for="validationBday">Rate</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend"></span>
                        </div>
                        <input type="text" class="form-control" id="validationBday" placeholder="Rate"
                            aria-describedby="inputGroupPrepend" name="rate" required>
                        <div class="invalid-feedback">
                            Please input a Rate.
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <label for="validationBday">Overtime</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend"></span>
                        </div>
                        <input type="number" class="form-control" id="validationBday" placeholder="Overtime"
                            aria-describedby="inputGroupPrepend" name="ot">
                    </div>
                </div>

            </div>
            <input class="btn btn-primary" type="submit" value="Add Employee" name="addEmp">
        </form>
    </div>
</body>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/validate.js"></script>
</body>

</html>